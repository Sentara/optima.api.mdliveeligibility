﻿using Optima.API.MDLiveEligibility.Models;
using CSC.Common;
using CSC.Common.DataAccess;
using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.Data.SqlClient;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Optima.API.MDLiveEligibility.Controllers
{
    public class MDLiveEligibilityController : ControllerBase
    {

        readonly IConfiguration _configuration;

        public MDLiveEligibilityController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [Route("api/MDLiveEligibility")]
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK, Description = "Get Eligibility of Member for MDLive", Type = typeof(MDLiveRequest))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Bad Request")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Not Found")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Internal server error")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "No Content")]
        public IActionResult Post([FromBody] MDLiveRequest mdliveRequest)
        {

            try
            {
                // If the ModelState is invalid, it means there was an error trying to deserialize the JSON into the MDLiveRequest model.
                if (!ModelState.IsValid)
                {
                    throw new Exception("Invalid request. The message cannot be deserialized.");
                }

                // Get the member id and date of birth from the Member section. We won't ever use the Subscriber section because we are determining eligibility of the Member, whether it's 
                // Self, Spouse, Child, etc.
                var memberId = mdliveRequest.Member.MemberId;
                var dateOfBirth = mdliveRequest.Member.DateOfBirth.Replace("-", "");

                // The date of service is always today.
                var dateOfService = DateTime.Today.ToString("yyyyMMdd");

                var isSEP = _configuration.GetValue<string>("IsSEP");

                // Get connection strings to connect to CSC and SQL databases.
                var cscConnString = (isSEP == "Yes")? _configuration.GetValue<string>("ConnectionStrings:CSC_SEP") : _configuration.GetValue<string>("ConnectionStrings:CSC");
                var ohwConnString = _configuration.GetValue<string>("ConnectionStrings:OptimaHealthWeb");
                var pdsConnString = _configuration.GetValue<string>("ConnectionStrings:PersonalizationDataServices"); 
 
                string cmdtext = string.Format("CALL GetMbrEligibility('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                    memberId, dateOfBirth, "", "", "", dateOfService, "OHWEBMED", "00501");

                // Set up the connection to CSC.
                var ccf = new CSCConnectionFactory(cscConnString, cmdtext);
                var da = new CSCDataAccess(ccf.Connection, ccf.DataAdapter);

                // Make the call and read the results into a datatable.
                var dt = da.Read(ccf.CommandText);

                // There should always be at least 1 row returned, because if the member is not found there will be a row with Result Indicator of 72. No rows is a system error.
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("No rows returned from call to GetMbrEligibility.");
                }

                var resultIndicator = Convert.ToString(dt.Rows[0]["RESULT_INDICATOR"]);

                // Member not found.
                if (resultIndicator == "72")
                {
                    MDLiveResponseError mdlrError = new MDLiveResponseError
                    {
                        Code = "subscriber_not_found",
                        Message = "We apologize, but the member information entered does not match any records with your health plan. Please verify the information entered matches what is printed on your health insurance card and try again."
                    };

                    MDLiveResponseErrorList ret = new MDLiveResponseErrorList
                    {
                        Errors = new List<MDLiveResponseError>()
                    };

                    ret.Errors.Add(mdlrError);

                    return Ok(ret);
                }

                // Member not eligible.
                if (resultIndicator == "62")
                {
                    MDLiveResponseError mdlrError = new MDLiveResponseError
                    {
                        Code = "subscriber_ineligible",
                        Message = "We apologize, but the member is ineligible for this benefit. Please verify your eligibility with your health plan provider or employer's plan benefit coordinator."
                    };

                    MDLiveResponseErrorList ret = new MDLiveResponseErrorList
                    {
                        Errors = new List<MDLiveResponseError>()
                    };

                    ret.Errors.Add(mdlrError);

                    return Ok(ret);
                }

                // Member found and eligible. Build a response to send back to MDLive.
                MDLiveResponse mdliveResponse = new MDLiveResponse
                {
                    MemberId = dt.Rows[0]["MEM_NO"].ToString(),
                    Relationship = "Self",
                    FirstName = dt.Rows[0]["FIRST_NAME"].ToString(),
                    MiddleInitial = (dt.Rows[0]["MID"].ToString().Length > 0 ? dt.Rows[0]["MID"].ToString().Substring(0, 1) : ""),
                    LastName = dt.Rows[0]["LAST_NAME"].ToString(),
                    DateOfBirth = DateTime.ParseExact(dt.Rows[0]["DOB"].ToString(), "yyyyMMdd", null).ToString("yyyy-MM-dd"),
                    Gender = dt.Rows[0]["GENDER"].ToString() == "M" ? "Male" : dt.Rows[0]["GENDER"].ToString() == "F" ? "Female" : "Unknown",
                    Address = new Address 
                    { 
                        Address1 = dt.Rows[0]["ADDR1"].ToString(),
                        Address2 = dt.Rows[0]["ADDR2"].ToString(),
                        City = dt.Rows[0]["CITY"].ToString(),
                        State = dt.Rows[0]["STATE"].ToString().Trim(),
                        ZipCode = dt.Rows[0]["ZIP"].ToString()
                    },
                    PrimaryPhone = dt.Rows[0]["PHONE"].ToString(),
                    PrimaryEmail = GetEmailAddress(memberId, ohwConnString, pdsConnString),
                    Eligible = "true",
                    EligibilityRange = new EligibilityRange
                    {
                        StartDate = DateTime.ParseExact(dt.Rows[0]["SUB_DATE"].ToString(), "yyyyMMdd", null).ToString("yyyy-MM-dd"),
                        EndDate = DateTime.Today.ToString("yyyy-MM-dd")
                    },
                    GroupId = dt.Rows[0]["GROUP_NO"].ToString(),
                    PlanId = dt.Rows[0]["PLAN"].ToString(),
                    PlanName = dt.Rows[0]["PLAN_DESC"].ToString(),
                    MemberCostShare = new MemberCostShare
                    {
                        Medical = "0.00",
                        Psychology = "0.00",
                        Psychiatry = "0.00",
                        Dermatology = "0.00"
                    }
                };

                return Ok(mdliveResponse);

            }
            catch (Exception ex)
            {
                MDLiveResponseError mdlrError = new MDLiveResponseError
                {
                    Code = "system_error",
                    Message = "We apologize, but we could not verify your benefits at this time."
                };

                MDLiveResponseErrorList ret = new MDLiveResponseErrorList
                {
                    Errors = new List<MDLiveResponseError>()
                };

                ret.Errors.Add(mdlrError);

                return Ok(ret);
            }
        }

        /// <summary>
        /// GetEmailAddress: Get the email address for the member by querying SQL databases directly.
        /// 1. Calls stproc OptimaHealthWeb..Member$GetByMbrID to get the PersonID for a given member.
        /// 2. Calls stproc PersonalizationDataServices..PersonEmailGetByType to get the email address for the person.
        /// </summary>
        /// <param name="memberId">Optima Health Member ID</param>
        /// <param name="ohwConnString">OptimaHealthWeb database connection string</param>
        /// <param name="pdsConnString">PersonalizationDataServices dataabase connection string</param>
        /// <returns>Email address for the member or empty string if it cannot be found.</returns>
        private string GetEmailAddress(string memberId, string ohwConnString, string pdsConnString)
        {
            int personId = 0;
            string emailAddress = "";

            using (SqlConnection connection = new SqlConnection(ohwConnString))
            {
                using (SqlCommand command = new SqlCommand("Member$GetByMbrID"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@MbrID", memberId);

                    command.Connection = connection;
                    connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return "";
                    }

                    reader.Read();
                    personId = Convert.ToInt32(reader["PersonID"]);

                    reader.Close();
                    connection.Close();
                }
            }

            using (SqlConnection connection = new SqlConnection(pdsConnString))
            {
                using (SqlCommand command = new SqlCommand("PersonEmailGetByType"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PersonID", personId);
                    command.Parameters.AddWithValue("@EmailTypeID", 1);

                    command.Connection = connection;
                    connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return "";
                    }

                    reader.Read();
                    emailAddress = reader["EmailAddress"].ToString();

                    reader.Close();
                    connection.Close();
                }
            }
           
            return emailAddress;
        }

    }
}
