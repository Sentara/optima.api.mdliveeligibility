﻿using System.Data;

namespace CSC.Common.DataAccess
{
    public class CSCDataAccess
    {
        private IDbConnection _con { get; set; }
        private IDbDataAdapter _da { get; set; }
        public CSCDataAccess(IDbConnection con, IDbDataAdapter da)
        {
            _con = con;
            _da = da;
        }

        public DataTable Read(string commandText)
        {
            var ds = new DataSet();
            using (_con)
            {
                _con.Open();
                IDbCommand cmd = _con.CreateCommand();
                cmd.CommandText = commandText;
                _da.SelectCommand = cmd;
                _da.Fill(ds);
            }
            return ds.Tables[0];
        }
    }
}
