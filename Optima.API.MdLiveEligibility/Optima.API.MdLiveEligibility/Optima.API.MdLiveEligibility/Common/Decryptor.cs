﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CSC.Common
{
    public class Decryptor
    {
        /// <summary>
        /// Decrypts a byte array using the specified key
        /// </summary>
        /// <param name="data">Encrypted data</param>
        /// <param name="key">Encryption key</param>
        /// <returns>A byte array containing the plain text
        /// version of the data</returns>
        public static byte[] Decrypt(byte[] data, byte[] key)
        {
            // extract the IV. IV length is 16 bytes
            int ivLen = 16;
            //int ivLen = 32;

            if (data == null || data.Length <= ivLen || key == null || key.Length == 0)
            {
                return data;
            }
            byte[] IV = new byte[ivLen];
            Array.Copy(data, IV, ivLen);

            // split out the IV from the data
            byte[] tmpData = new byte[data.Length - ivLen];
            Array.Copy(data, ivLen, tmpData, 0, tmpData.Length);

            //Set up the memory stream for the decrypted data.
            MemoryStream memStream = new MemoryStream();

            //Pass in the initialization vector.
            ICryptoTransform transform = GetCryptoServiceProvider(key, IV);

            CryptoStream decStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            try
            {
                decStream.Write(tmpData, 0, tmpData.Length);
            }
            finally
            {
                decStream.FlushFinalBlock();
                decStream.Close();
            }

            return memStream.ToArray();
        }

        /// <summary>
        /// Decrypts a base64-encoded encrypted string using the
        /// specified key.
        /// </summary>
        /// <param name="data">Base64 encoded encrypted string</param>
        /// <param name="key">Encryption key</param>
        /// <returns>A plain text ASCII version of the data</returns>
        public static string Decrypt(string data, string key)
        {
            if (string.IsNullOrEmpty(data) || string.IsNullOrEmpty(key))
            {
                return data;
            }

            byte[] bytesData = Convert.FromBase64String(data);
            byte[] bytesKey = Convert.FromBase64String(key);

            return Encoding.ASCII.GetString(Decrypt(bytesData, bytesKey));
        }

        /// <summary>
        /// Provides a cryptographic transformer. Currently returns one for AES
        /// </summary>
        /// <param name="key">The symmetric encryption key.</param>
        /// <param name="IV">Initialization vector</param>
        /// <returns></returns>
        internal static ICryptoTransform GetCryptoServiceProvider(byte[] key, byte[] IV)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (IV == null)
            {
                throw new ArgumentNullException("IV");
            }

            int validKeyLengthBytes = 256 / 8;
            //int validKeyLengthBytes = 128 / 8;

            if (key.Length < validKeyLengthBytes)
            {
                throw new ArgumentException("Invalid key size.");
            }

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider
            {
                Mode = CipherMode.CBC
            };

            //aes.BlockSize = 128;
            //aes.Padding = PaddingMode.PKCS7;
            //aes.IV = IV;

            return aes.CreateDecryptor(key, IV);
        }
    }
}