﻿using System.Data;
using System.Data.Odbc;

namespace CSC.Common
{
	public class CSCConnectionFactory
	{
		public IDbDataAdapter DataAdapter { get; set; }
		public IDbConnection Connection { get; set; }
		public string CommandText { get; set; }

		public CSCConnectionFactory(string connectionString, string cmdtext)
		{
			Connection = new OdbcConnection(connectionString);
			DataAdapter = new OdbcDataAdapter();
			CommandText = "{" + cmdtext + "}";
		}
	}
}