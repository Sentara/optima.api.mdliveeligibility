﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optima.API.MDLiveEligibility.Models
{

    public class MDLiveResponseErrorList
    {
        [JsonProperty("errors")]
        public List<MDLiveResponseError> Errors;
    }

    public class MDLiveResponseError
    {

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

    }
}