﻿using System.Text.Json.Serialization;

namespace Optima.API.MDLiveEligibility.Models
{

    public class Address
    {
        [JsonPropertyName("address_1")]
        public string Address1 { get; set; }

        [JsonPropertyName("address_2")]
        public string Address2 { get; set; }

        [JsonPropertyName("city")]
        public string City { get; set; }

        [JsonPropertyName("state")]
        public string State { get; set; }

        [JsonPropertyName("zipcode")]
        public string ZipCode { get; set; }

    }

    public class EligibilityRange
    {
        [JsonPropertyName("start_date")]
        public string StartDate { get; set; }

        [JsonPropertyName("end_date")]
        public string EndDate { get; set; }
    }

    public class MemberCostShare
    {
        [JsonPropertyName("medical")]
        public string Medical { get; set; }

        [JsonPropertyName("psychology")]
        public string Psychology { get; set; }

        [JsonPropertyName("psychiatry")]
        public string  Psychiatry { get; set; }

        [JsonPropertyName("dermatology")]
        public string Dermatology { get; set; }
    }

    public class MDLiveResponse
    {
        [JsonPropertyName("member_id")]
        public string MemberId { get; set; }

        [JsonPropertyName("relationship")]
        public string Relationship { get; set; }

        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }

        [JsonPropertyName("middle_initial")]
        public string MiddleInitial { get; set; }

        [JsonPropertyName("last_name")]
        public string LastName { get; set; }

        [JsonPropertyName("date_of_birth")]
        public string DateOfBirth { get; set; }

        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonPropertyName("address")]
        public Address Address { get; set; }

        [JsonPropertyName("primary_phone")]
        public string PrimaryPhone { get; set; }

        [JsonPropertyName("primary_email")]
        public string PrimaryEmail { get; set; }

        [JsonPropertyName("eligible")]
        public string Eligible { get; set; }

        [JsonPropertyName("eligibility_range")]
        public EligibilityRange EligibilityRange { get; set; }

        [JsonPropertyName("group_id")]
        public string GroupId { get; set; }

        [JsonPropertyName("plan_id")]
        public string PlanId { get; set; }

        [JsonPropertyName("plan_name")]
        public string PlanName { get; set; }

        [JsonPropertyName("member_cost_share")]
        public MemberCostShare MemberCostShare { get; set; }

    }
}