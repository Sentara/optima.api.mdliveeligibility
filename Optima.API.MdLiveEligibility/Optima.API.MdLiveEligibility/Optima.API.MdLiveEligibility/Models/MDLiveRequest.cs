﻿using System.Text.Json.Serialization;

namespace Optima.API.MDLiveEligibility.Models
{
    public class Member
    {
        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }

        [JsonPropertyName("last_name")]
        public string LastName { get; set; }

        [JsonPropertyName("date_of_birth")]
        public string DateOfBirth { get; set; }

        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonPropertyName("member_id")]
        public string MemberId { get; set; }

        [JsonPropertyName("relationship")]
        public string Relationship { get; set; }

        [JsonPropertyName("charges")]
        public Charges Charges { get; set; }
    }

    public class Subscriber
    {
        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }

        [JsonPropertyName("last_name")]
        public string LastName { get; set; }

        [JsonPropertyName("date_of_birth")]
        public string DateOfBirth { get; set; }

        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonPropertyName("member_id")]
        public string MemberId { get; set; }
    }

    public class Charges
    {
        [JsonPropertyName("medical")]
        public string Medical { get; set; }

        [JsonPropertyName("psychology")]
        public string Psychology { get; set; }

        [JsonPropertyName("psychiatry")]
        public string Psychiatry { get; set; }

        [JsonPropertyName("dermatology")]
        public string Dermatology { get; set; }
    }

    public class MDLiveRequest
    {
        [JsonPropertyName("member")]
        public Member Member { get; set; }

        [JsonPropertyName("subscriber")]
        public Subscriber Subscriber { get; set; }
    }
}