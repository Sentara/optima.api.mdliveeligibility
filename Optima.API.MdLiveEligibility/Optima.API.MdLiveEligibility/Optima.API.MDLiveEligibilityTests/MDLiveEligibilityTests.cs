﻿using Xunit;
using Optima.API.MDLiveEligibility.Controllers;
using Optima.API.MDLiveEligibility.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Optima.API.MDLiveEligibility.Tests
{
    public class MDLiveEligibilityTests
    {
        IConfiguration _configuration;
        MDLiveEligibilityController _controller;


        public MDLiveEligibilityTests()
        {
            var myConfiguration = new Dictionary<string, string>
            {
                {"IsSEP", "No"},
                {"ConnectionStrings:CSC", "Dsn=CSC;Uid=OPT.ODBC;Pwd=4sentara"},
                {"ConnectionStrings:OptimaHealthWeb", "data source=PORTALSDEVDB01.corplab.adlab.sentara.labin;initial catalog=OptimaHealthWeb;user=SVCSQL_WEBAPI1;password=6YAvwc7G6nUNDzXG"},
                {"ConnectionStrings:PersonalizationDataServices", "data source=PORTALSDEVDB01.corplab.adlab.sentara.labin;initial catalog=PersonalizationDataServices;user=SVCSQL_WEBAPI1;password=6YAvwc7G6nUNDzXG"},
            };

            _configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();

            _controller = new MDLiveEligibilityController(_configuration);
        }

        [Fact]
        public void Member_Eligible()
        {
        
            MDLiveRequest request = new MDLiveRequest()
            {
                Member = new Member
                {
                    MemberId = "4662083*01",
                    DateOfBirth = "1973-08-06"
                },
                Subscriber = new Subscriber
                {

                }
            };

            var actionResult = _controller.Post(request) as OkObjectResult;

            Assert.NotNull(actionResult);
            Assert.Equal(200, actionResult.StatusCode);

            Assert.IsType<MDLiveResponse>(actionResult.Value);

            var contentResult = actionResult.Value as MDLiveResponse;
                        
            Assert.Equal("true", contentResult.Eligible);
            Assert.Equal("HADJINLIAN", contentResult.LastName);

        }

        [Fact]
        public void Member_Not_Eligible()
        {

            MDLiveRequest request = new MDLiveRequest()
            {
                Member = new Member
                {
                    MemberId = "1103663*01",
                    DateOfBirth = "1975-12-08"
                },
                Subscriber = new Subscriber
                {

                }
            };

            var actionResult = _controller.Post(request) as OkObjectResult;

            Assert.NotNull(actionResult);
            Assert.Equal(200, actionResult.StatusCode);

            Assert.IsType<MDLiveResponseErrorList>(actionResult.Value);

            var contentResult = actionResult.Value as MDLiveResponseErrorList;

            Assert.NotNull(contentResult.Errors[0]);
            Assert.Equal("subscriber_ineligible", contentResult.Errors[0].Code);

        }

        [Fact]
        public void Member_Not_Found()
        {

            MDLiveRequest request = new MDLiveRequest()
            {
                Member = new Member
                {
                    MemberId = "9991234*01",
                    DateOfBirth = "1966-02-02"
                },
                Subscriber = new Subscriber
                {

                }
            };

            var actionResult = _controller.Post(request) as OkObjectResult;

            Assert.NotNull(actionResult);
            Assert.Equal(200, actionResult.StatusCode);

            Assert.IsType<MDLiveResponseErrorList>(actionResult.Value);

            var contentResult = actionResult.Value as MDLiveResponseErrorList;

            Assert.NotNull(contentResult.Errors[0]);
            Assert.Equal("subscriber_not_found", contentResult.Errors[0].Code);

        }

    }
}
